# juicy2-interface

An even shinier and newer version of the Juicy 2 interface written with React
and Redux, written more betterer this time around. The cool thing about this
version is that none of the UI components contain any state. Therefore, the UI
itself is a pure function. Woo woo!

[You can try the demo here.](http://juicy2.surge.sh/)

## To run:

```bash
$ git clone https://gitlab.com/docmenthol/juicy2-interface.git
$ cd juicy2-interface
$ npm install
$ npm start
```
