const round = (number, precision) => {
  var factor = Math.pow(10, precision)
  var tempNumber = number * factor
  var roundedTempNumber = Math.round(tempNumber)
  return roundedTempNumber / factor
}

const amt = (ta, p) => {
  const n = round(ta * (p / 100), 2)
  return isNaN(n) ? '--' : n
}

const calculate = (parameters, flavors) => {
  // Calculate the recipe
  const fp = flavors.reduce((a, b) => a + b.percent, 0)
  const fa = parameters.targetAmount * (fp / 100)
  const na = round(parameters.targetAmount * (parameters.targetStrength / parameters.nicStrength), 2)
  const ba = round(parameters.targetAmount - (na + fa), 2)
  const naw = round(na * 1.23, 2)

  const vg = round((parameters.vgPercent / 100) * ba, 2)
  const pg = round((parameters.pgPercent / 100) * ba, 2)
  const vgw = round(vg * 1.26, 2)
  const pgw = round(pg * 1.04, 2)

  let rows = flavors.map(f => {
    const a = amt(parameters.targetAmount, f.percent)
    return { ...f, name: f.name || 'Unnamed Flavor', amount: a, weight: a }
  })

  // Add in the nicotine base and glycerin base amounts before returning
  rows.push({ name: 'Nic', amount: na, weight: naw, id: -1 })
  if (vg > 0) rows.push({ name: 'VG Base', amount: vg, weight: vgw, id: -2 })
  if (pg > 0) rows.push({ name: 'PG Base', amount: pg, weight: pgw, id: -3 })

  return rows
}

const tryNumber = n =>
  isNaN(parseFloat(n.toString().charAt(n.length - 1)))
    ? n
    : parseFloat(n) === parseInt(n, 10)
      ? parseInt(n, 10)
      : parseFloat(n)

export default calculate
export { round, amt, tryNumber }
