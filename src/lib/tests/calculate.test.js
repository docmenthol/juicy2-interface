import * as c from '../calculate'

describe('calulate library works', () => {
  it('rounds correctly', () =>
    expect(c.round(1.23, 1)).toEqual(1.2))

  it('calculates amounts correctly', () =>
    expect(c.amt(10, 1)).toEqual(0.1))

  it('tries numbers correctly', () =>
    expect(c.tryNumber('10.0')).toEqual(10))

  it('tries numbers and gets strings correctly', () =>
    expect(c.tryNumber('10.')).toEqual('10.'))
})
