const setLoading = loading => ({ type: 'interim/SET_LOADING', loading })
const unsetLoading = () => ({ type: 'interim/UNSET_LOADING' })
const setSaving = saving => ({ type: 'interim/SET_SAVING', saving })
const unsetSaving = () => ({ type: 'interim/UNSET_SAVING' })

export { setLoading, unsetLoading, setSaving, unsetSaving }
