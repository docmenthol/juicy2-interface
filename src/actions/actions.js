import { bindActionCreators } from 'redux'
import active from './active'
import flavors from './flavors'
import inputs from './inputs'
import loading from './loading'
import modals from './modals'
import parameters from './parameters'
import recipes from './recipes'

const actions = bindActionCreators({
  active,
  flavors,
  inputs,
  loading,
  modals,
  parameters,
  recipes
})

export default actions
