const updateNicStrength = strength => ({ type: 'SET_NIC_STRENGTH', strength })
const updateTargetStrength = strength => ({ type: 'SET_TARGET_STRENGTH', strength })
const updateTargetAmount = amount => ({ type: 'SET_TARGET_AMOUNT', amount })
const updateRatio = ratio => ({ type: 'SET_RATIO', ratio })

export { updateNicStrength, updateTargetStrength, updateTargetAmount, updateRatio }
