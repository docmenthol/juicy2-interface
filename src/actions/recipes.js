const saveRecipe = recipe => ({ type: 'SAVE_RECIPE', recipe })
const deleteRecipe = recipe => ({ type: 'DELETE_RECIPE', recipe })

export { saveRecipe, deleteRecipe }
