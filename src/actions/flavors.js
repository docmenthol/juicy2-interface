const addFlavor = () => ({ type: 'ADD_FLAVOR' })
const deleteFlavor = flavor => ({ type: 'DELETE_FLAVOR', flavor })
const updateFlavor = (flavor, diff) => ({ type: 'UPDATE_FLAVOR', flavor, diff })
const clearFlavors = () => ({ type: 'CLEAR_FLAVORS' })
const setFlavors = flavors => ({ type: 'SET_FLAVORS', flavors })

export { addFlavor, deleteFlavor, updateFlavor, clearFlavors, setFlavors }
