const openModal = modal => ({ type: 'OPEN_MODAL', modal })
const closeModal = modal => ({ type: 'CLOSE_MODAL', modal })

export { openModal, closeModal }
