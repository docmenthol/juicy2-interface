const setPlatform = platform => ({ type: 'SET_PLATFORM', platform })

export { setPlatform }
