const setLoading = loading => ({ type: 'SET_LOADING', loading })
const unsetLoading = () => ({ type: 'UNSET_LOADING' })

export { setLoading, unsetLoading }
