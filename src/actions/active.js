const setActive = active => ({ type: 'SET_ACTIVE', active })
const unsetActive = () => ({ type: 'UNSET_ACTIVE' })

export { setActive, unsetActive }
