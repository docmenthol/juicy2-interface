import { setPlatform } from '../platform'

describe('platform actions', () => {
  it('returns a valid action', () =>
    expect(setPlatform('a')).toEqual({type: 'SET_PLATFORM', platform: 'a'}))
})
