import { updateInput, clearInput } from '../inputs'

describe('inputs actions', () => {
  it('returns a valid action', () =>
    expect(updateInput('a', 'b'))
      .toEqual({type: 'UPDATE_INPUT', input: 'a', value: 'b'}))

  it('returns a valid action', () =>
    expect(clearInput('a')).toEqual({type: 'CLEAR_INPUT', input: 'a'}))
})
