import * as p from '../parameters'

describe('parameters actions', () => {
  it('returns a valid action', () =>
    expect(p.updateNicStrength(1))
      .toEqual({type: 'SET_NIC_STRENGTH', strength: 1}))

  it('returns a valid action', () =>
    expect(p.updateTargetStrength(1))
      .toEqual({type: 'SET_TARGET_STRENGTH', strength: 1}))

  it('returns a valid action', () =>
    expect(p.updateTargetAmount(1))
      .toEqual({type: 'SET_TARGET_AMOUNT', amount: 1}))
})
