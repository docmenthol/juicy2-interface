import * as f from '../flavors'

describe('flavors actions', () => {
  it('returns a valid action', () =>
    expect(f.addFlavor()).toEqual({type: 'ADD_FLAVOR'}))

  it('returns a valid action', () =>
    expect(f.deleteFlavor('a')).toEqual({type: 'DELETE_FLAVOR', flavor: 'a'}))

  it('returns a valid action', () =>
    expect(f.updateFlavor('a', 'b'))
      .toEqual({type: 'UPDATE_FLAVOR', flavor: 'a', diff: 'b'}))

  it('returns a valid action', () =>
    expect(f.clearFlavors()).toEqual({type: 'CLEAR_FLAVORS'}))

  it('returns a valid action', () =>
    expect(f.setFlavors('a')).toEqual({type: 'SET_FLAVORS', flavors: 'a'}))
})
