import { saveRecipe, deleteRecipe } from '../recipes'

describe('recipes actions', () => {
  it('returns a valid action', () =>
    expect(saveRecipe('a')).toEqual({type: 'SAVE_RECIPE', recipe: 'a'}))

  it('returns a valid action', () =>
    expect(deleteRecipe('a')).toEqual({type: 'DELETE_RECIPE', recipe: 'a'}))
})
