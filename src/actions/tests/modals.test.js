import { openModal, closeModal } from '../modals'

describe('modals actions', () => {
  it('returns a valid action', () =>
    expect(openModal('a')).toEqual({type: 'OPEN_MODAL', modal: 'a'}))

  it('returns a valid action', () =>
    expect(closeModal('a')).toEqual({type: 'CLOSE_MODAL', modal: 'a'}))
})
