import { setLoading, unsetLoading } from '../loading'

describe('loading actions', () => {
  it('returns a valid action', () =>
    expect(setLoading('a')).toEqual({type: 'SET_LOADING', loading: 'a'}))

  it('returns a valid action', () =>
    expect(unsetLoading()).toEqual({type: 'UNSET_LOADING'}))
})
