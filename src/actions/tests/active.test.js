import { setActive, unsetActive } from '../active'

describe('active actions', () => {
  it('returns a valid action', () =>
    expect(setActive('a')).toEqual({type: 'SET_ACTIVE', active: 'a'}))

  it('returns a valid action', () =>
    expect(unsetActive()).toEqual({type: 'UNSET_ACTIVE'}))
})
