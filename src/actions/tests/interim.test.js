import * as i from '../interim'

describe('interim actions', () => {
  it('returns a valid action', () =>
    expect(i.setLoading('a')).toEqual({type: 'interim/SET_LOADING', loading: 'a'}))

  it('returns a valid action', () =>
    expect(i.unsetLoading()).toEqual({type: 'interim/UNSET_LOADING'}))

  it('returns a valid action', () =>
    expect(i.setSaving('a')).toEqual({type: 'interim/SET_SAVING', saving: 'a'}))

  it('returns a valid action', () =>
    expect(i.unsetSaving()).toEqual({type: 'interim/UNSET_SAVING'}))
})
