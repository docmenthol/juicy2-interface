const updateInput = (input, value) => ({ type: 'UPDATE_INPUT', input, value })
const clearInput = input => ({ type: 'CLEAR_INPUT', input })

export { updateInput, clearInput }
