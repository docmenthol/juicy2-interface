import { connect } from 'react-redux'
import { updateNicStrength, updateTargetStrength, updateTargetAmount, updateRatio } from '../../actions/parameters'
import ParameterInput from '../../components/Mixer/ParameterInput'

const mapStateToProps = state => ({
  parameters: state.parameters
})

const mapDispatchToProps = dispatch => ({
  updateNicStrength: strength => dispatch(updateNicStrength(strength)),
  updateTargetStrength: strength => dispatch(updateTargetStrength(strength)),
  updateTargetAmount: amount => dispatch(updateTargetAmount(amount)),
  updateRatio: ratio => dispatch(updateRatio(ratio))
})

export default connect(mapStateToProps, mapDispatchToProps)(ParameterInput)
