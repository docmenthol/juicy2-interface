import { connect } from 'react-redux'
import { addFlavor } from '../../actions/flavors'
import Mixer from '../../components/Mixer'

const mapStateToProps = state => ({
  flavors: state.flavors,
  active: state.active,
  parameters: state.parameters
})

const mapDispatchToProps = dispatch => ({
  addFlavor: () => dispatch(addFlavor())
})

export default connect(mapStateToProps, mapDispatchToProps)(Mixer)
