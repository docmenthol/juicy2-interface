import { connect } from 'react-redux'
import Output from '../../components/Mixer/Output'
import calculate from '../../lib/calculate'

const mapStateToProps = state => {
  const { parameters, flavors } = state
  const ingredients = calculate(parameters, flavors)
  return { ingredients }
}

export default connect(mapStateToProps)(Output)
