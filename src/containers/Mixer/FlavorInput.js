import { connect } from 'react-redux'
import { updateFlavor, deleteFlavor } from '../../actions/flavors'
import FlavorInput from '../../components/Mixer/FlavorInput'

const mapStateToProps = state => ({})

const mapDispatchToProps = (dispatch) => ({
  updateFlavor: (flavor, diff) => dispatch(updateFlavor(flavor, diff)),
  deleteFlavor: flavor => dispatch(deleteFlavor(flavor))
})

export default connect(mapStateToProps, mapDispatchToProps)(FlavorInput)
