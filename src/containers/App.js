import { connect } from 'react-redux'
import { openModal } from '../actions/modals'
import { setLoading } from '../actions/loading'
import App from '../components/App'

const mapStateToProps = state => ({
  parameters: state.parameters,
  flavors: state.flavors,
  recipes: state.recipes,
  active: state.active,
  modals: state.modals
})

const mapDispatchToProps = dispatch => ({
  loadRecipe: (i, recipes) => {
    dispatch(setLoading(recipes[i]))
    dispatch(openModal('load'))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
