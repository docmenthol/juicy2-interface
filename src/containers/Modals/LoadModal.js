import { connect } from 'react-redux'
import { setFlavors} from '../../actions/flavors'
import { closeModal } from '../../actions/modals'
import { setActive } from '../../actions/active'
import { unsetLoading } from '../../actions/loading'
import LoadModal from '../../components/Modals/LoadModal'

const mapStateToProps = state => ({
  visible: state.modals.load,
  loading: state.loading
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => {
    dispatch(unsetLoading())
    dispatch(closeModal('load'))
  },
  loadRecipe: loading => {
    dispatch(setFlavors(loading.flavors))
    dispatch(setActive({ name: loading.name, id: loading.id, notes: loading.notes }))
    dispatch(unsetLoading())
    dispatch(closeModal('load'))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(LoadModal)
