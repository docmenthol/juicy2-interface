import { connect } from 'react-redux'
import { closeModal } from '../../actions/modals'
import { clearInput } from '../../actions/inputs'
import { saveRecipe } from '../../actions/recipes'
import { setActive } from '../../actions/active'
import ConfirmSaveModal from '../../components/Modals/ConfirmSaveModal'

const mapStateToProps = state => ({
  visible: state.modals.confirmSave,
  flavors: state.flavors,
  name: state.inputs.name,
  notes: state.inputs.notes
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal('confirmSave')),
  clearInput: () => {
    dispatch(clearInput('name'))
    dispatch(clearInput('notes'))
  },
  saveRecipe: recipe => {
    dispatch(saveRecipe(recipe))
    dispatch(setActive(recipe))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmSaveModal)
