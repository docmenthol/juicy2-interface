import { connect } from 'react-redux'
import { clearFlavors } from '../../actions/flavors'
import { deleteRecipe } from '../../actions/recipes'
import { closeModal } from '../../actions/modals'
import { unsetActive } from '../../actions/active'
import DeleteModal from '../../components/Modals/DeleteModal'

const mapStateToProps = state => ({
  visible: state.modals.delete,
  active: state.active
})

const mapDispatchToProps = dispatch => ({
  deleteRecipe: recipe => {
    dispatch(clearFlavors())
    dispatch(deleteRecipe(recipe))
    dispatch(unsetActive())
  },
  closeModal: () => dispatch(closeModal('delete'))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeleteModal)
