import { connect } from 'react-redux'
import { closeModal } from '../../actions/modals'
import { updateInput, clearInput } from '../../actions/inputs'
import { saveRecipe } from '../../actions/recipes'
import LoadFromJSONModal from '../../components/Modals/LoadFromJSONModal'

const mapStateToProps = state => ({
  visible: state.modals.json,
  name: state.inputs.name,
  json: state.inputs.json
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal('json')),
  updateName: e => dispatch(updateInput('name', e.target.value)),
  updateJson: e => dispatch(updateInput('json', e.target.value)),
  clearInput: () => {
    dispatch(clearInput('name'))
    dispatch(clearInput('json'))
  },
  saveRecipe: (name, flavors) => dispatch(saveRecipe(name, flavors))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoadFromJSONModal)
