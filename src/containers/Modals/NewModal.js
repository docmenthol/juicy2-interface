import { connect } from 'react-redux'
import { clearFlavors} from '../../actions/flavors'
import { closeModal } from '../../actions/modals'
import { unsetActive } from '../../actions/active'
import NewModal from '../../components/Modals/NewModal'

const mapStateToProps = state => ({
  visible: state.modals.new
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal('new')),
  clearRecipe: () => {
    dispatch(clearFlavors())
    dispatch(unsetActive())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(NewModal)
