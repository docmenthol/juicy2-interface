import { connect } from 'react-redux'
import { saveRecipe } from '../../actions/recipes'
import { setActive } from '../../actions/active'
import { updateInput, clearInput } from '../../actions/inputs'
import { openModal, closeModal } from '../../actions/modals'
import SaveModal from '../../components/Modals/SaveModal'

const mapStateToProps = state => ({
  visible: state.modals.save,
  flavors: state.flavors,
  name: state.inputs.name,
  notes: state.inputs.notes,
  recipes: state.recipes
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal('save')),
  updateName: e => dispatch(updateInput('name', e.target.value)),
  updateNotes: e => dispatch(updateInput('notes', e.target.value)),
  clearInput: () => {
    dispatch(clearInput('name'))
    dispatch(clearInput('notes'))
  },
  saveRecipe: recipe => {
    dispatch(saveRecipe(recipe))
    dispatch(setActive(recipe))
  },
  openNextModal: () => dispatch(openModal('confirmSave'))
})

const mergeProps = (state, dispatch) => ({
  checkAndSaveRecipe: recipe => {
    const { name } = state
    const match = state.recipes.filter(r => r.name === name).length > 0
    if (match) {
      dispatch.closeModal()
      dispatch.openNextModal()
    } else {
      const { flavors, notes } = state
      dispatch.saveRecipe({ flavors, name, notes })
      dispatch.closeModal()
    }
  },
  ...state, ...dispatch
})

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SaveModal)
