import { connect } from 'react-redux'
import { openModal } from '../../actions/modals'
import LoadFromJSONButton from '../../components/Buttons/LoadFromJSONButton'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  openModal: () => dispatch(openModal('json'))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoadFromJSONButton)
