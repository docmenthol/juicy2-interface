import { connect } from 'react-redux'
import { openModal } from '../../actions/modals'
import SaveButton from '../../components/Buttons/SaveButton'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  openModal: () => dispatch(openModal('save'))
})

export default connect(mapStateToProps, mapDispatchToProps)(SaveButton)
