import { connect } from 'react-redux'
import { openModal } from '../../actions/modals'
import NewButton from '../../components/Buttons/NewButton'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  openModal: () => dispatch(openModal('new'))
})

export default connect(mapStateToProps, mapDispatchToProps)(NewButton)
