import { connect } from 'react-redux'
import { openModal } from '../../actions/modals'
import DeleteButton from '../../components/Buttons/DeleteButton'

const mapStateToProps = state => ({
  active: state.active
})

const mapDispatchToProps = dispatch => ({
  openModal: () => dispatch(openModal('delete'))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeleteButton)
