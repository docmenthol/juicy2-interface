const flavors = (flavors = [], action) => {
  switch (action.type) {
    case 'ADD_FLAVOR':
      const newFlavor = {
        name: '',
        percent: 0,
        vgPercent: 0,
        pgPercent: 100,
        id: Math.floor(Math.random() * 1000000)
      }
      return [ ...flavors, newFlavor ]
    case 'DELETE_FLAVOR':
      return flavors.filter(f => f.id !== action.flavor.id)
    case 'UPDATE_FLAVOR':
      let newFlavors = [ ...flavors ]
      let i = newFlavors.findIndex(f => f.id === action.flavor.id)
      newFlavors[i] = { ...action.flavor, ...action.diff }
      return newFlavors
    case 'CLEAR_FLAVORS':
      return []
    case 'SET_FLAVORS':
      return action.flavors
    default:
      return flavors
  }
}

export default flavors
