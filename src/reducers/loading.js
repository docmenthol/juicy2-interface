const loading = (loading = null, action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return action.loading
    case 'UNSET_LOADING':
      return null
    default:
      return loading
  }
}

export default loading
