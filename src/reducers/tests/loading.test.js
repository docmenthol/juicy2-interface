import loading from '../loading'
import { setLoading, unsetLoading } from '../../actions/loading'

describe('loading reducer', () => {
  it('returns a valid starting state', () =>
    expect(loading(null, {type: 'TEST'})).toEqual(null))

  it('returns a valid new state when setting loading', () =>
    expect(loading(null, setLoading('a'))).toEqual('a'))

  it('returns a valid new state when unsetting loading', () =>
    expect(loading(1, unsetLoading())).toEqual(null))
})
