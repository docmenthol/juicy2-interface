import recipes from '../recipes'
import { saveRecipe, deleteRecipe } from '../../actions/recipes'

describe('recipes reducer', () => {
  it('returns a valid starting state', () =>
    expect(recipes([], {type: 'TEST'})).toEqual([]))

  it('returns a valid new state when saving', () =>
    expect(recipes([], saveRecipe({name: 'a'})))
      .toMatchObject([
        expect.objectContaining({
          name: 'a',
          id: expect.any(Number)
        })
      ]))

  it('returns a valid new state when deleting', () =>
    expect(recipes([{id: 1}], deleteRecipe({id: 1}))).toEqual([]))
})
