import interim from '../interim'
import * as i from '../../actions/interim'

describe('interim reducer', () => {
  it('returns a valid starting state', () =>
    expect(interim({}, {type: 'TEST'})).toEqual({}))

  it('returns a valid new state when setting loading', () =>
    expect(interim({}, i.setLoading('a'))).toEqual({loading: 'a'}))

  it('returns a valid new state when unsetting loading', () =>
    expect(interim({loading: 'a'}, i.unsetLoading())).toEqual({loading: null}))

  it('returns a valid new state when setting saving', () =>
    expect(interim({}, i.setSaving('a'))).toEqual({saving: 'a'}))

  it('returns a valid new state when unsetting saving', () =>
    expect(interim({saving: 'a'}, i.unsetSaving())).toEqual({saving: null}))
})
