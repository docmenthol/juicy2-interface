import flavors from '../flavors'
import * as f from '../../actions/flavors'

describe('flavors reducer', () => {
  it('returns a valid starting state', () =>
    expect(flavors([], {type: 'TEST'})).toEqual([]))

  it('returns a valid new state when adding a flavor', () =>
    expect(flavors([], f.addFlavor({name: 'a', percent: 1})))
      .toMatchObject([expect.objectContaining({name: '', percent: 0, id: expect.any(Number)})]))

  it('returns a valid new state when deleting a flavor', () =>
    expect(flavors(['a'], f.deleteFlavor('a'))).toEqual([]))

  it('returns a valid new state when updating a flavor', () =>
    expect(flavors([{id: 1, name: 'a'}], f.updateFlavor({id: 1}, {name: 'b'})))
      .toEqual([{id: 1, name: 'b'}]))

  it('returns a valid new state when clearing flavors', () =>
    expect(flavors(['a'], f.clearFlavors())).toEqual([]))

  it('returns a valid new state setting flavors', () =>
    expect(flavors([], f.setFlavors(['a']))).toEqual(['a']))
})
