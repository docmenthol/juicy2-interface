import inputs from '../inputs'
import { updateInput, clearInput } from '../../actions/inputs'

describe('inputs reducer', () => {
  it('returns a valid starting state', () =>
    expect(inputs({}, {type: 'TEST'})).toEqual({}))

  it('returns a valid new state when setting an input', () =>
    expect(inputs({}, updateInput('a', 1))).toEqual({'a': 1}))

  it('returns a valid new state when unsetting an input', () =>
    expect(inputs({'a': 1}, clearInput('a'))).toEqual({'a': null}))
})
