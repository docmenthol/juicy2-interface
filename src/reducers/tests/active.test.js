import active from '../active'
import { setActive, unsetActive } from '../../actions/active'

describe('active reducer', () => {
  it('returns a valid starting state', () =>
    expect(active(null, {type: 'TEST'})).toEqual(null))

  it('returns a valid new state when setting active', () =>
    expect(active(null, setActive('a'))).toEqual('a'))

  it('returns a valid new state when unsetting active', () =>
    expect(active(1, unsetActive())).toEqual(null))
})
