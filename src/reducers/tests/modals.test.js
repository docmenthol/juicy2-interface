import modals from '../modals'
import { openModal, closeModal } from '../../actions/modals'

describe('modals reducer', () => {
  it('returns a valid starting state', () =>
    expect(modals(null, {type: 'TEST'})).toEqual(null))

  it('returns a valid new state when opening a modal', () =>
    expect(modals({'a': false}, openModal('a'))).toEqual({'a': true}))

  it('returns a valid new state when closing a modal', () =>
    expect(modals({'a': true}, closeModal('a'))).toEqual({'a': false}))
})
