import parameters from '../parameters'
import * as p from '../../actions/parameters'

describe('parameters reducer', () => {
  it('returns a valid starting state', () =>
    expect(parameters(null, {type: 'TEST'})).toEqual(null))

  it('returns a valid new state when updating nic strength', () =>
    expect(parameters({}, p.updateNicStrength(1))).toEqual({nicStrength: 1}))

  it('returns a valid new state when updating target strength', () =>
    expect(parameters({}, p.updateTargetStrength(1))).toEqual({targetStrength: 1}))

  it('returns a valid new state when updating target amount', () =>
    expect(parameters({}, p.updateTargetAmount(1))).toEqual({targetAmount: 1}))
})
