const inputs = (inputs = {}, action) => {
  switch (action.type) {
    case 'UPDATE_INPUT':
      let updatedInputs = { ...inputs }
      updatedInputs[action.input] = action.value
      return updatedInputs
    case 'CLEAR_INPUT':
      let newInputs = { ...inputs }
      newInputs[action.input] = null
      return newInputs
    default:
      return inputs
  }
}

export default inputs
