const modals = (modals = {}, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      let openedModal = { ...modals }
      openedModal[action.modal] = true
      return openedModal
    case 'CLOSE_MODAL':
      let closedModal = { ...modals }
      closedModal[action.modal] = false
      return closedModal
    default:
      return modals
  }
}

export default modals
