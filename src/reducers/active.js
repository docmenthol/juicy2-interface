const active = (active = null, action) => {
  switch (action.type) {
    case 'SET_ACTIVE':
      return action.active
    case 'UNSET_ACTIVE':
      return null
    default:
      return active
  }
}

export default active
