const recipes = (recipes = [], action) => {
  switch (action.type) {
    case 'SAVE_RECIPE':
      const cleaned = recipes.filter(r => r.name !== action.recipe.name)
      return [ ...cleaned, { ...action.recipe, id: Math.floor(Math.random() * 1000000) } ]
    case 'DELETE_RECIPE':
      return recipes.filter(r => r.id !== action.recipe.id)
    default:
      return recipes
  }
}

export default recipes
