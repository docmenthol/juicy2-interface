const parameters = (parameters = {}, action) => {
  switch (action.type) {
    case 'SET_NIC_STRENGTH':
      return { ...parameters, nicStrength: action.strength }
    case 'SET_TARGET_STRENGTH':
      return { ...parameters, targetStrength: action.strength }
    case 'SET_TARGET_AMOUNT':
      return { ...parameters, targetAmount: action.amount }
    case 'SET_RATIO':
      return { ...parameters, ...action.ratio }
    default:
      return parameters
  }
}

export default parameters
