const interim = (interim = {}, action) => {
  switch (action.type) {
    case 'interim/SET_LOADING':
      return { ...interim, loading: action.loading}
    case 'interim/UNSET_LOADING':
      return { ...interim, loading: null }
    case 'interim/SET_SAVING':
      return { ...interim, saving: action.saving}
    case 'interim/UNSET_SAVING':
      return { ...interim, saving: null }
    default:
      return interim
  }
}

export default interim
