import React from 'react'
import { Layout, Menu, Icon } from 'antd'
import { HashRouter as Router, Route, Link } from 'react-router-dom'
import Mixer from '../containers/Mixer'
// import Selector from './Selector'
import LoadFromJSONButton from '../containers/Buttons/LoadFromJSONButton'

const { Header, Footer, Sider } = Layout

const App = ({ version = '0.0.0', parameters, flavors, recipes, active, modals, loadRecipe }) => {
  let sortedRecipes = [ ...recipes ]
  sortedRecipes.sort((a, b) => a.name > b.name)
  const recipeItems = sortedRecipes.map((r, i) =>
    <Menu.Item key={i}>
      <Link to={`/mixer/${r.id}`}>
        {r.name}
      </Link>
    </Menu.Item>)

  // const coilItems = []

  // this should probably be split up into a composable layout in some way
  return (
    <Router>
      <Layout>
        <Header>
          <img src='favicon.png' alt='Juicy 2' style={{ height: 60 }} />
        </Header>
        <Layout>
          <Sider>
            <Menu mode='inline' defaultOpenKeys={['recipes']} onClick={e => loadRecipe(e.key, recipes)}>
              <Menu.SubMenu title='Recipes' key='recipes'>
                { recipeItems }
              </Menu.SubMenu>
              {/*
              <Menu.SubMenu title='Coils' key='coils'>
                { coilItems }
              </Menu.SubMenu>
              */}
            </Menu>
            <LoadFromJSONButton />
          </Sider>

          <Route exact path='/' component={Mixer} />
          {/* <Route exact path='/mixer' component={Mixer} /> */}
          <Route exact path='/mixer/:recipeID' component={Mixer} />

        </Layout>
        <Footer style={{ textAlign: 'center' }}>
          v{version}<br />
          <a href='https://gitlab.com/docmenthol/juicy2-interface/' target='_new'><Icon type='fork' style={{ color: 'rgba(0, 0, 0, 0.65)' }} /></a>
        </Footer>

      </Layout>
    </Router>
  )
}

export default App
