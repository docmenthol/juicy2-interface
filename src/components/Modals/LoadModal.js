import React from 'react'
import { Modal } from 'antd'

const LoadModal = ({ loading, loadRecipe, closeModal, ...others }) =>
  <Modal
    title='Load Recipe'
    onOk={() => {
      loadRecipe(loading)
    }}
    onCancel={closeModal}
    okText='Yes'
    cancelText='Nevermind'
    {...others}
  >
    Are you sure you want to load this recipe?
  </Modal>

export default LoadModal
