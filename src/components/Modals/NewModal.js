import React from 'react'
import { Modal } from 'antd'

const NewModal = ({ clearRecipe, closeModal, ...others }) =>
  <Modal
    title='New Recipe'
    onOk={() => {
      clearRecipe()
      closeModal()
    }}
    onCancel={closeModal}
    okText='Yes'
    cancelText='Nevermind'
    {...others}
  >
    Are you sure you want to clear these flavors?
  </Modal>

export default NewModal
