import React from 'react'
import { Modal, Input } from 'antd'

const SaveModal = ({ flavors, name, notes, closeModal, saveRecipe, updateName, updateNotes, clearInput, checkAndSaveRecipe, ...others }) =>
  <Modal
    title='Save Recipe'
    okText='Save'
    cancelText='Nevermind'
    onOk={() => checkAndSaveRecipe({ flavors, name, notes })}
    onCancel={() => {
      clearInput()
      closeModal()
    }}
    {...others}
  >
    <Input.Group size='large'>
      <Input placeholder='Recipe name.' onChange={updateName} style={{ marginBottom: '1em' }} />
      <Input.TextArea placeholder='Recipe notes.' onChange={updateNotes} autosize={{ minRows: 5, maxRows: 10 }} />
    </Input.Group>
  </Modal>

export default SaveModal
