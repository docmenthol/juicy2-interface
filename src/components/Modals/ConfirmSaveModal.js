import React from 'react'
import { Modal } from 'antd'

const ConfirmSaveModal = ({ flavors, name, notes, saveRecipe, clearInput, closeModal, ...others }) =>
  <Modal
    title='Save Recipe'
    onOk={() => {
      saveRecipe({ flavors, name, notes })
      clearInput()
      closeModal()
    }}
    onCancel={closeModal}
    okText='Yes'
    cancelText='Nevermind'
    {...others}
  >
    Are you sure you want to overwrite { name }?
  </Modal>

export default ConfirmSaveModal
