import React from 'react'
import { Modal, Input } from 'antd'

const LoadFromJSONModal = ({ json, name, closeModal, updateName, updateJson, clearInput, saveRecipe, ...others }) =>
  <Modal
    title='Load Recipe From JSON'
    okText='Load'
    cancelText='Nevermind'
    onOk={() => {
      const flavors = JSON.parse(json).flavors.map(f => ({ ...f, id: Math.floor(Math.random() * 1000000) }))
      saveRecipe(flavors, name)
      clearInput()
      closeModal()
    }}
    onCancel={() => {
      clearInput()
      closeModal()
    }}
    {...others}
>
    <Input.Group size='large'>
      <Input placeholder='Recipe Name' onChange={updateName} style={{ marginBottom: '1em' }} />
      <Input.TextArea placeholder='Recipe JSON' onChange={updateJson} autosize={{ minRows: 5, maxRows: 10 }} />
    </Input.Group>
  </Modal>

export default LoadFromJSONModal
