import React from 'react'
import { Modal } from 'antd'

const DeleteModal = ({ active, deleteRecipe, closeModal, ...others }) =>
  <Modal
    title='Delete Recipe'
    onOk={() => {
      deleteRecipe(active)
      closeModal()
    }}
    onCancel={closeModal}
    okText='Yes'
    cancelText='Nevermind'
    {...others}
  >
    Are you sure you want to delete this recipe?
  </Modal>

export default DeleteModal
