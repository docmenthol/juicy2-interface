import React from 'react'
import { Layout, Breadcrumb, Row, Col, Card } from 'antd'
import { Link } from 'react-router-dom'

const { Content } = Layout

const Selector = props =>
  <Layout style={{ padding: '0 24px 24px' }}>
    <Breadcrumb style={{ margin: '12px 0' }}>
      <Breadcrumb.Item>Selector</Breadcrumb.Item>
    </Breadcrumb>
    <Content>
      <Row gutter={25}>
        <Col span={6}>
          <Link to='/mixer'>
            <Card title='Juice Mixer'>Mix juice recipes.</Card>
          </Link>
        </Col>
        <Col span={6}>
          <Link to='/'>
            <Card title='Coin Builder'>Calculate your perfect coils.</Card>
          </Link>
        </Col>
      </Row>
    </Content>
  </Layout>

export default Selector
