import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Input } from 'antd'
import { tryNumber } from '../../lib/calculate'

const ParameterInput = ({
  parameters,
  updateNicStrength,
  updateTargetStrength,
  updateTargetAmount,
  updateRatio
}) =>
  <Row gutter={16} justify='center' style={{ paddingBottom: '1em' }}>
    <Input.Group size='large'>
      <Col span={6}>
        <Input
          placeholder='Nicotine Strength'
          addonAfter='mg'
          value={parameters.nicStrength}
          onChange={e => updateNicStrength(tryNumber(e.target.value))}
        />
      </Col>
      <Col span={6}>
        <Input
          size='large'
          placeholder='Target Strength'
          addonAfter='mg'
          value={parameters.targetStrength}
          onChange={e => updateTargetStrength(tryNumber(e.target.value))}
        />
      </Col>
      <Col span={6}>
        <Input
          size='large'
          placeholder='Target Amount'
          addonAfter='mL'
          value={parameters.targetAmount}
          onChange={e => updateTargetAmount(tryNumber(e.target.value))}
        />
      </Col>
      <Col span={3}>
        <Input
          size='large'
          value={parameters.vgPercent}
          addonBefore='VG'
          addonAfter='%'
          onChange={e =>
            updateRatio(
              {
                vgPercent: tryNumber(e.target.value),
                pgPercent: 100 - tryNumber(e.target.value)
              })}
        />
      </Col>
      <Col span={3}>
        <Input
          size='large'
          value={parameters.pgPercent}
          addonBefore='PG'
          addonAfter='%'
          onChange={e =>
            updateRatio(
              {
                vgPercent: 100 - tryNumber(e.target.value),
                pgPercent: tryNumber(e.target.value)
              })}
        />
      </Col>
    </Input.Group>
  </Row>

ParameterInput.propTypes = {
  parameters: PropTypes.shape({
    nicStrength: PropTypes.number,
    targetStrength: PropTypes.number,
    targetAmount: PropTypes.number
  }).isRequired,
  updateNicStrength: PropTypes.func.isRequired,
  updateTargetStrength: PropTypes.func.isRequired,
  updateTargetAmount: PropTypes.func.isRequired
}

export default ParameterInput
