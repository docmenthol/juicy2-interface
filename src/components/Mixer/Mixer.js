import React from 'react'
import { Layout, Breadcrumb, Row, Col, Steps } from 'antd'
import MixerInput from './MixerInput'
import Output from '../../containers/Mixer/Output'
import SaveModal from '../../containers/Modals/SaveModal'
import ConfirmSaveModal from '../../containers/Modals/ConfirmSaveModal'
import NewModal from '../../containers/Modals/NewModal'
import LoadModal from '../../containers/Modals/LoadModal'
import DeleteModal from '../../containers/Modals/DeleteModal'
import LoadFromJSONModal from '../../containers/Modals/LoadFromJSONModal'

const { Content } = Layout
const { Step } = Steps

const readyState = (parameters, flavors) => {
  // only the amount needs to be greater than zero -- some people vape nic-free
  const paramsReady = parameters.targetAmount > 0

  // gotta have at least one flavor with a name and an amount
  const flavorsReady = flavors.map(f => f.name).some(n => n.length > 0) && flavors.map(f => f.percent).some(p => p > 0)

  return paramsReady && !flavorsReady ? 1 : (paramsReady && flavorsReady ? 2 : 0)
}

const Mixer = ({ active, parameters, flavors, match, addFlavor }) => {
  if (match.params.hasOwnProperty('recipeID')) {
    const { recipeID } = match.params
    console.log(`Would load recipe ${recipeID}.`)
  }
  const currentStep = readyState(parameters, flavors)
  const crumb =
    active
      ? <Breadcrumb.Item>{ active.name }</Breadcrumb.Item>
      : <Breadcrumb.Item>Untitled Recipe</Breadcrumb.Item>

  return (
    <Layout style={{ padding: '0 24px 24px' }}>
      <Breadcrumb style={{ margin: '12px 0' }}>
        <Breadcrumb.Item>Mixer</Breadcrumb.Item>
        { crumb }
      </Breadcrumb>
      <Content style={{ background: '#fff', padding: 24, margin: 0 }}>
        <Row style={{ paddingBottom: '2em' }}>
          <Col span={24}>
            <Steps current={currentStep}>
              <Step title='Parameters' description='Set recipe basic parameters.' />
              <Step title='Flavors' description='Add some flavors.' />
              <Step title='Mix' description='Mix recipe!' />
            </Steps>
          </Col>
        </Row>
        <Row gutter={32}>
          <Col span={12}>
            <MixerInput
              flavors={flavors}
              active={active}
              addFlavor={addFlavor}
            />
          </Col>
          <Col span={12}><Output /></Col>
        </Row>
      </Content>

      {/* all modals here so they'll only ever be rendered once, ever */}
      <SaveModal />
      <ConfirmSaveModal />
      <NewModal />
      <LoadModal />
      <DeleteModal />
      <LoadFromJSONModal />
    </Layout>
  )
}

export default Mixer
