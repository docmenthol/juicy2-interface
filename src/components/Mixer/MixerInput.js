import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Button, Icon, Tooltip } from 'antd'
import ParameterInput from '../../containers/Mixer/ParameterInput'
import FlavorInput from '../../containers/Mixer/FlavorInput'
import SaveButton from '../../containers/Buttons/SaveButton'
import NewButton from '../../containers/Buttons/NewButton'
import DeleteButton from '../../containers/Buttons/DeleteButton'

const MixerInput = ({ flavors, active, addFlavor }) => {
  const flavorRows = flavors.map((f, i) => <FlavorInput flavor={f} key={i} />)
  const notes = active && active.notes
    ? <Row>
      <Col span={24}>
        <strong>Notes:</strong> { active.notes }
      </Col>
    </Row>
    : null
  return (
    <div>
      <Row gutter={16}>
        <Col span={20}>
          <ParameterInput />
        </Col>
        <Col span={2}>
          <Button.Group size='large'>
            <NewButton />
            <SaveButton />
          </Button.Group>
        </Col>
        <Col span={1}>
          <DeleteButton />
        </Col>
      </Row>
      <Row>{ flavorRows }</Row>
      <Row style={{ paddingBottom: '1em' }}>
        <Col span={4}>
          <Tooltip placement='bottom' title='Add Flavor'>
            <Button onClick={addFlavor}>
              <Icon type='plus' />
            </Button>
          </Tooltip>
        </Col>
      </Row>
      { notes }
    </div>
  )
}

MixerInput.propTypes = {
  flavors: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      percent: PropTypes.number,
      id: PropTypes.number.isRequired
    })
  ).isRequired,
  active: PropTypes.shape({
    flavors: PropTypes.array,
    name: PropTypes.string,
    notes: PropTypes.string,
    id: PropTypes.number
  })
}

export default MixerInput
