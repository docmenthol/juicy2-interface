import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'

const Columns = [
  {
    title: 'Ingredient',
    dataIndex: 'name',
    key: 'name'
  }, {
    title: 'Amount (mL)',
    dataIndex: 'amount',
    key: 'volume'
  },
  {
    title: 'Amount (g)',
    dataIndex: 'weight',
    key: 'weight'
  }
]

const Output = ({ ingredients, parameters }) => <Table dataSource={ingredients} columns={Columns} pagination={false} rowKey='id' />

Output.propTypes = {
  ingredients: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      percent: PropTypes.number,
      amount: PropTypes.number,
      id: PropTypes.number.isRequired
    })
  ).isRequired,
  parameters: PropTypes.shape({
    nicStrength: PropTypes.number,
    targetStrength: PropTypes.number,
    targetAmount: PropTypes.number
  })
}

export default Output
