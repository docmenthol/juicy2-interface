import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Input, Button, Icon, Tooltip } from 'antd'
import { tryNumber } from '../../lib/calculate'

const FlavorInput = ({ flavor, updateFlavor, deleteFlavor }) =>
  <Row gutter={16} justify='center' style={{ paddingBottom: '1em' }}>
    <Col span={20}>
      <Input
        size='large'
        placeholder='Flavor Name'
        value={flavor.name}
        onChange={e => updateFlavor(flavor, { name: e.target.value })}
    />
    </Col>
    <Col span={2}>
      <Input
        size='large'
        addonAfter='%'
        value={flavor.percent}
        onChange={e =>
          updateFlavor(flavor, { percent: tryNumber(e.target.value) })}
    />
    </Col>
    <Col span={2}>
      <Tooltip placement='right' title='Delete Flavor'>
        <Button size='large' onClick={e => deleteFlavor(flavor)}>
          <Icon type='delete' />
        </Button>
      </Tooltip>
    </Col>
  </Row>

FlavorInput.propTypes = {
  flavor: PropTypes.shape({
    name: PropTypes.string,
    percent: PropTypes.number,
    id: PropTypes.number.isRequired
  }).isRequired,
  updateFlavor: PropTypes.func.isRequired,
  deleteFlavor: PropTypes.func.isRequired
}

export default FlavorInput
