import React from 'react'
import { Button, Icon, Tooltip } from 'antd'

const NewButton = ({ openModal }) =>
  <Tooltip placement='top' title='New Recipe'>
    <Button size='large' onClick={openModal}>
      <Icon type='file-add' />
    </Button>
  </Tooltip>

export default NewButton
