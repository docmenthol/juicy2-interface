import React from 'react'
import { Button, Icon, Tooltip } from 'antd'

const DeleteButton = ({ active, openModal }) =>
  active
    ? <Tooltip placement='top' title={`Delete ${active.name}`}>
      <Button size='large' onClick={openModal}>
        <Icon type='delete' />
      </Button>
    </Tooltip> : null

export default DeleteButton
