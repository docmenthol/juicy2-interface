import React from 'react'
import { Button, Tooltip, Icon } from 'antd'

const LoadFromJSONButton = ({ openModal }) =>
  <Tooltip placement='bottom' title='Load Recipe From JSON'>
    <Button onClick={openModal} style={{ marginTop: '1em', marginLeft: '35%' }} shape='circle'>
      <Icon type='upload' />
    </Button>
  </Tooltip>

export default LoadFromJSONButton
