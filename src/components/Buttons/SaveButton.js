import React from 'react'
import { Button, Icon, Tooltip } from 'antd'

const SaveButton = ({ flavors, openModal }) =>
  <Tooltip placement='top' title='Save Recipe'>
    <Button size='large' onClick={openModal}>
      <Icon type='save' />
    </Button>
  </Tooltip>

export default SaveButton
