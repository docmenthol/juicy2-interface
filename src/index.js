import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import * as storage from 'redux-storage'
import createEngine from 'redux-storage-engine-localstorage'
import reducers from './reducers/reducers'
import initialState from './initialState'
import App from './containers/App'
import registerServiceWorker from './registerServiceWorker'
import 'antd/dist/antd.css'

const VERSION = '0.8.2'

const filtered = ['modals', 'inputs', 'loading']
const replacer = (k, v) => filtered.indexOf(k) !== -1 ? undefined : v

const reducer = storage.reducer(reducers)
const engine = createEngine('juicy2', replacer)
const engineMiddleware = storage.createMiddleware(engine, [
  'SET_LOADING', 'UNSET_LOADING',
  'OPEN_MODAL', 'CLOSE_MODAL',
  'UPDATE_INPUT', 'CLEAR_INPUT'
])
const createStoreWithMiddleware = applyMiddleware(engineMiddleware)(createStore)

let appStore = createStoreWithMiddleware(
  reducer, initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const load = storage.createLoader(engine)
load(appStore)
    .then((newState) => console.log('Loaded state:', newState))
    .catch(() => console.log('No initial state found.'))

ReactDOM.render(
  <Provider store={appStore}>
    <App version={VERSION} />
  </Provider>, document.getElementById('root'))
registerServiceWorker()
