export default {
  flavors: [],
  parameters: {
    nicStrength: null,
    targetStrength: null,
    targetAmount: null,
    vgPercent: 100,
    pgPercent: 0
  },
  recipes: [],
  modals: {
    save: false,
    new: false,
    load: false,
    delete: false,
    json: false,
    confirmSave: false
  },
  inputs: {
    name: null,
    notes: null,
    json: null
  },
  active: null,
  loading: null
}
